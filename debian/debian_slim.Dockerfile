FROM debian:11-slim

# Update and install base
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
  jq jo wget curl zip unzip \
  && rm -rf /var/lib/apt/lists/*

COPY vault.sh /usr/local/bin/vault.sh
