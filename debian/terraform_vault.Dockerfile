FROM debian:11-slim

# Update and install base
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
  jq jo wget gpg lsb-release curl zip unzip \
  && rm -rf /var/lib/apt/lists/*

# Install hashicorp
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg \
  && gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint \
  && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list \
  && apt-get update \
  && apt-get install -y \
  vault terraform \
  && rm -rf /var/lib/apt/lists/*
