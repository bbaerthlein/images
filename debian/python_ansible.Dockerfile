ARG img_from

FROM ${img_from}

RUN apt-get update && apt-get install -y \
    python3-minimal \
    python3-venv \
    python3-pip \
    sshpass \
    rsync \
    && rm -rf /var/lib/apt/lists/*

ENV ANSIBLE_HOST_KEY_CHECKING False

RUN pip install ansible-core==2.15.13 docker \
    && ansible-galaxy collection install ansible.posix community.general community.docker
