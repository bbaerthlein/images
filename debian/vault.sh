#!/bin/bash
set -o pipefail

if ! jq --version &>/dev/null && jo --version &>/dev/null; then
  >&2 echo "This scripts needs jq and jo to be installed!"
  return 1
fi

function approle-login() {
  _payload=$(jo \
    -p role_id="${VAULT_ROLE_ID}" \
    -p secret_id="${VAULT_SECRET_ID}")
  _json=$(curl -fs -X POST \
    --data "${_payload}" \
    "${VAULT_ADDR}/v1/auth/approle/login")
  _err=$?
  if [ $_err -ne 0 ]; then
    >&2 echo "curl returned error $_err: $_json"
    return 1
  fi
  if ! _tmp=$(echo "${_json}" | jq -Mre ".auth.client_token"); then
    >&2 echo "Did not find auth token in response"
    return 1
  fi
  >&2 echo "Successfully logged into vault with approle"
  export VAULT_TOKEN="${_tmp}"
}

function jwt-login() {
  _payload=$(jo \
    -p role="${VAULT_AUTH_ROLE}" \
    -p jwt="${1:-${CI_JOB_JWT}}")
  _json=$(curl -fs -X POST \
    --data "${_payload}" \
    "${VAULT_ADDR}/v1/auth/jwt/login")
  _err=$?
  if [ $_err -ne 0 ]; then
    >&2 echo "curl returned error $_err: $_json"
    return 1
  fi
  if ! _tmp=$(echo "${_json}" | jq -Mre ".auth.client_token"); then
    >&2 echo "Did not find auth token in response"
    return 1
  fi
  >&2 echo "Successfully logged into vault with jwt"
  export VAULT_TOKEN="${_tmp}"
}

function ldap-login() {
  if [ -z "${VAULT_LDAP_USER}" ]; then
    echo -n "Username: "
    if ! read VAULT_LDAP_USER; then
      return 1
    fi
  fi
  if [ -z "${VAULT_LDAP_PASSWORD}" ]; then
    echo -n "Password: "
    if ! read -s VAULT_LDAP_PASSWORD; then
      return 1
    fi
  fi
  _payload=$(jo -p password="${VAULT_LDAP_PASSWORD}")
  _json=$(curl -fs -X POST \
    --data "${_payload}" \
      "${VAULT_ADDR}/v1/auth/ldap/login/${VAULT_LDAP_USER}")
  _err=$?
  if [ $_err -ne 0 ]; then
    >&2 echo "curl returned error $_err: $_json"
    return 1
  fi
  if ! _tmp=$(echo "${_json}" | jq -Mre ".auth.client_token"); then
    >&2 echo "Did not find auth token in response"
    return 1
  fi
  >&2 echo "Successfully logged into vault with ldap"
  export VAULT_TOKEN="${_tmp}"
}

function _split_kv_path() {
  export engine=${1%%/*}
  local _rest=${1#*/}
  export field=${_rest##*/}
  export path=${_rest%/*}

  if [[ -z "${engine}" || -z "${path}" || -z "${field}" ]]; then
    >&2 echo "Invalid or empty secret path: '${1}'"
    return 1
  fi
}

function kv-get() {
  _split_kv_path "$1"

  _json=$(curl -fs -X GET \
    -H "X-Vault-Token: ${VAULT_TOKEN}" \
    "${VAULT_ADDR}/v1/${engine}/data/${path}")
  _err=$?
  if [ $_err -ne 0 ]; then
    >&2 echo "curl returned error ${_err}: ${_json}"
    return 1
  fi

  echo "${_json}" | jq -Mre ".data.data.${field}"
  _err=$?
  if [ $_err -ne 0 ]; then
    >&2 echo "jq returned error $_err"
    return 1
  fi
}

function kv-export() {
  local _tmp
  if ! _tmp=$(kv-get "$2"); then
    unset "$1"
    return 1
  fi
  export "$1"="${_tmp}"
}

function kv-file() {
local _tmp
  if ! _tmp=$(kv-get "$2"); then
    unset "$1"
    return 1
  fi
  mkdir -p $(dirname $1)
  echo "${_tmp}" > "$1"
  if [ -n "$3" ]; then
    chmod "$3" "$1"
  fi
}

# Call function
if [[ -z "$1" || $(type -t "${1}") != function ]]; then
  echo "Usage: ${0} cmd [parameters]"
  echo "approle-login"
  echo "ldap-login"
  echo "jwt-login [jwt]"
  echo "kv-get path"
  echo "kv-export var path"
  echo "kv-file file path (mod)"
fi
_func=$1; shift 1
eval "${_func} $*"
